package com.test.twino.homework.service;

import com.test.twino.homework.dto.LoanDto;
import com.test.twino.homework.entity.Loan;
import com.test.twino.homework.exception.ResourceNotFoundException;
import com.test.twino.homework.repository.LoanRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@SpringBootTest
public class LoanServiceTest {

    @Autowired
    private LoanService loanService;

    @MockBean
    private LoanRepository loanRepository;

    @Test
    @DisplayName("Test findById Success")
    public void should_ReturnData_WhenCallFindById() {
        // GIVEN
        Loan loan = generateLoan("123","Tuan", "Le");
        doReturn(Optional.of(loan)).when(loanRepository).findById(1);

        //WHEN
        Optional<Loan> returnedLoan = loanService.findById(1);

        // THEN
        Assertions.assertTrue(returnedLoan.isPresent(), "Loan was found");
        Assertions.assertSame(returnedLoan.get(), loan, "The loan returned was the same as the mock");
    }

    @Test
    @DisplayName("Test findById not found")
    public void should_DataEmpty_WhenCallFindById() {
        // GIVEN
        doReturn(Optional.empty()).when(loanRepository).findById(1);

        //WHEN
        Optional<Loan> returnedLoan = loanService.findById(1);

        // THEN
        Assertions.assertFalse(returnedLoan.isPresent(), "Loan was not found");
    }

    @Test
    @DisplayName("Test findAll Success")
    public void should_ReturnData_WhenCallFindAll() {
        // GIVEN
        Loan loan1 = generateLoan("123","Tuan","Le");
        Loan loan2 = generateLoan("456","Tom","Nguyen");
        String sortBy = "personalId";
        String orderBy ="ASC";
        doReturn(Arrays.asList(loan1,loan2)).when(loanRepository).findAll(Sort.by(Sort.Direction.valueOf(orderBy),sortBy));

        //WHEN
        List<Loan> loans = loanService.findAll(sortBy, orderBy);

        // THEN
        Assertions.assertEquals(2, loans.size(), "findAll should return 2 loan");
    }

    @Test
    @DisplayName("Test findAll do not have data")
    public void should_DataEmpty_WhenCallFindAll() {
        // GIVEN
        String sortBy = "personalId";
        String orderBy ="ASC";
        doReturn(Arrays.asList()).when(loanRepository).findAll(Sort.by(Sort.Direction.valueOf(orderBy),sortBy));

        //WHEN
        List<Loan> loans = loanService.findAll(sortBy, orderBy);

        // THEN
        Assertions.assertEquals(0, loans.size(), "findAll should not have data");
    }

    @Test
    @DisplayName("Test save loan")
    public void should_CreateLoan_WhenCallCreateMethod() {
        // GIVEN
        Loan loan = generateLoan();
        doReturn(loan).when(loanRepository).save(any());

        // WHEN
        LoanDto loanDto = generateLoanDto();
        Loan returnedLoan = loanService.create(loanDto);

        // THEN
        Assertions.assertNotNull(returnedLoan, "The created Loan should not be null");
        Assertions.assertEquals("Tuan", returnedLoan.getFirstName(), "The first name is the same with mock");
    }

    @Test
    @DisplayName("Test update column mark in loan")
    public void should_updateMarkValue_WhenCallUpdateMethod() throws ResourceNotFoundException {
        // GIVEN
        Loan loan = generateLoan();
        doReturn(Optional.of(loan)).when(loanRepository).findById(1);

        // WHEN
        Loan updatedLoan = loanService.updateMarkById(1, "Approved");

        // THEN
        Assertions.assertNotNull(updatedLoan, "The updated Loan should not be null");
        Assertions.assertEquals("Approved", updatedLoan.getMark(), "The mark column value is the same with mock");
    }

    private Loan generateLoan(String personnalId, String firstName, String lastName) {
        Loan loan = new Loan();
        loan.setPersonalID(personnalId);
        loan.setFirstName(firstName);
        loan.setLastName(lastName);
        return loan;
    }

    private Loan generateLoan() {
        return Loan.builder()
                .id(1)
                .personalID("123")
                .firstName("Tuan")
                .lastName("Le")
                .birthDate(LocalDate.of(1986,10,10))
                .salary(12500)
                .monthlyLiability(150)
                .requestedAmount(250)
                .requestedTerm(20)
                .mark("Rejected")
                .build();
    }

    private LoanDto generateLoanDto() {
        return LoanDto.builder()
                .personalID("123")
                .firstName("Tuan")
                .lastName("Le")
                .birthDate(LocalDate.of(1986,10,10))
                .salary(12500)
                .monthlyLiability(150)
                .requestedAmount(250)
                .requestedTerm(20)
                .build();
    }
}
