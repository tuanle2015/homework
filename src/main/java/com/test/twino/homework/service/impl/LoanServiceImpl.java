package com.test.twino.homework.service.impl;

import com.test.twino.homework.dto.LoanDto;
import com.test.twino.homework.entity.Loan;
import com.test.twino.homework.exception.ResourceNotFoundException;
import com.test.twino.homework.repository.LoanRepository;
import com.test.twino.homework.service.LoanService;
import com.test.twino.homework.utility.MarkScoreLabel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

@Service
public class LoanServiceImpl implements LoanService {

    private LoanRepository loanRepository;

    public LoanServiceImpl(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    @Override
    public Optional<Loan> findById(int id) {
        return loanRepository.findById(id);
    }

    @Override
    public List<Loan> findAll(String sortBy, String orderBy) {
        return loanRepository.findAll(Sort.by(Sort.Direction.valueOf(orderBy),sortBy));
    }

    @Override
    public Loan create(LoanDto loanDto) {
        Loan newLoan = convertToLoanEntity(loanDto);
        double creditScore = calculateScore(loanDto);
        newLoan.setCreditScore(creditScore);
        newLoan.setMark(evaluateMark(creditScore));

        return loanRepository.save(newLoan);
    }

    @Override
    public Loan updateMarkById(int loanId, String mark) throws ResourceNotFoundException {
        Loan existing = loanRepository.findById(loanId)
                .orElseThrow(() -> new ResourceNotFoundException("Loan application not found for this id :: " + loanId));
        existing.setMark(mark);
        loanRepository.save(existing);
        return existing;
    }

    @Override
    public void deleteById(int id)  throws ResourceNotFoundException{
        loanRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Loan application not found for this id: " + id));
        loanRepository.deleteById(id);
    }

    private Loan convertToLoanEntity(LoanDto loanDto) {
        Loan newLoan = Loan.builder()
                .personalID(loanDto.getPersonalID())
                .firstName(loanDto.getFirstName())
                .lastName(loanDto.getLastName())
                .birthDate(loanDto.getBirthDate())
                .employer(loanDto.getEmployer())
                .salary(loanDto.getSalary())
                .monthlyLiability(loanDto.getMonthlyLiability())
                .requestedAmount(loanDto.getRequestedAmount())
                .requestedTerm(loanDto.getRequestedTerm())
                .createdBy(loanDto.getCreatedBy())
                .createdDate(new Date())
                .build();

        return newLoan;
    }

    private String evaluateMark(double creditScore) {
        if (creditScore < 2500) {
            return MarkScoreLabel.REJECTED.getMark();
        } else if (creditScore > 3500) {
            return MarkScoreLabel.APPROVED.getMark();
        }

        return MarkScoreLabel.MANUAL.getMark();
    }

    private double calculateScore(LoanDto loanDto) {
        double score = (calAlphabet(loanDto.getFirstName())
                        + (loanDto.getSalary() *1.5)
                        - (loanDto.getMonthlyLiability() * 3)
                        + (loanDto.getBirthDate().getYear())
                        - (loanDto.getBirthDate().getMonthValue())
                        - calculateJulianDayOfYear(loanDto.getBirthDate()));
        return score;
    }

    private int calAlphabet(String word) {
        int sum = 0;
        for(char letter: word.toLowerCase().toCharArray()) {
            if (letter >= 'a' && letter <= 'z') {
                sum += (int) letter - 96;
            }
        }
        return sum;
    }

    private int calculateJulianDayOfYear(LocalDate birthDate) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.set(GregorianCalendar.DAY_OF_MONTH, birthDate.getDayOfMonth());
        gc.set(GregorianCalendar.MONTH, birthDate.getMonthValue());
        gc.set(GregorianCalendar.YEAR, birthDate.getYear());
        return gc.get(GregorianCalendar.DAY_OF_YEAR);
    }

}
