package com.test.twino.homework.utility;

public enum MarkScoreLabel {
    APPROVED("Approved"),
    MANUAL("Manual"),
    REJECTED("Rejected");

    private String mark;
    MarkScoreLabel(String mark) {
        this.mark = mark;
    }

    public String getMark() {
        return mark;
    }
}
