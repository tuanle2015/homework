package com.test.twino.homework.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.time.LocalDate;

@Data
@Builder
public class LoanDto {

    private int id;

    @NonNull
    private String personalID;

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    @NonNull
    private LocalDate birthDate;

    private String employer;

    @NonNull
    private double salary;

    @NonNull
    private double monthlyLiability;

    @NonNull
    private double requestedAmount;

    @NonNull
    private int requestedTerm;

    private int createdBy;
}
