package com.test.twino.homework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeworkApplication.class, args);
	}



//	@Bean
//	public CommandLineRunner demo(EmployeeRepository repository) {
//		return (args) -> {
//			// save a few customers
//			repository.save(new Employee("Jack", "Bauer", "Jack@gmail.com"));
//			repository.save(new Employee("Chloe", "O'Brian", "Chloe@gmail.com"));
//			repository.save(new Employee("Kim", "Bauer", "Kim@gmail.com"));
//			repository.save(new Employee("David", "Palmer", "David@gmail.com"));
//			repository.save(new Employee("Michelle", "Dessler", "Michelle@gmail.com"));
//
//		};
//	}

}
