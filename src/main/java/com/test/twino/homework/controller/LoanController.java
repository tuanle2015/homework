package com.test.twino.homework.controller;

import com.test.twino.homework.dto.LoanDto;
import com.test.twino.homework.entity.Loan;
import com.test.twino.homework.exception.ResourceNotFoundException;
import com.test.twino.homework.response.RestEnvelope;
import com.test.twino.homework.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/loan")
public class LoanController {

    @Autowired
    private LoanService loanService;

    @GetMapping("/{id}")
    public ResponseEntity<RestEnvelope> getLoanById(@PathVariable("id") int id) {
        Optional<Loan> loanApplication = loanService.findById(id);
        return new ResponseEntity<>(new RestEnvelope(loanApplication), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<RestEnvelope> getAllLoan(@RequestParam(required = false, defaultValue = "id") String sortBy,
                                                    @RequestParam(required = false, defaultValue = "ASC") String orderBy) {
        List<Loan> loanList = loanService.findAll(sortBy, orderBy);
        return new ResponseEntity<>(new RestEnvelope(loanList), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<RestEnvelope> create(@Validated @RequestBody LoanDto loanDto) {
        Loan loanCreated = loanService.create(loanDto);
        return new ResponseEntity<>(new RestEnvelope(loanCreated), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<RestEnvelope> update(@PathVariable("id") int id,
                                               @RequestParam(required = true) String mark) throws ResourceNotFoundException {
        Loan existing = loanService.updateMarkById(id, mark);
        return new ResponseEntity<>(new RestEnvelope(existing), HttpStatus.OK);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<RestEnvelope> delete(@PathVariable("id") int id) throws ResourceNotFoundException {
        loanService.deleteById(id);
        return new ResponseEntity<>(new RestEnvelope("Delete successfully"), HttpStatus.OK);
    }


}
